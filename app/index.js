var Peer = require('simple-peer')

const calc = require('./calc')
const numbersToAdd = [
    3,
    4,
    10,
    2
]
const result = calc.sum(numbersToAdd)
console.log(`The result is: ${result}`);

if (Peer.WEBRTC_SUPPORT) {
  // webrtc support!
  console.log("webrtc support!")
} else {
  // fallback
  console.log("fallback")
}
/*setInterval(() => {
	console.log("Test")
}, 1000);*/