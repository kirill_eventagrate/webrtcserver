//require('./app/index');
const Express = require('express')
const https = require('https')
const http = require('http')
const bodyParser = require('body-parser')
const fs = require('fs');
var wrtc = require('wrtc')
const DefaultRTCPeerConnection = require('wrtc');

function CreatePeerConnection(_type, _userId, _iceCandidateId) {
    var Peer = new DefaultRTCPeerConnection.RTCPeerConnection({});
    Peer.addEventListener('icecandidate', e => onIceCandidate(_type, Peer, e, _userId, _iceCandidateId));
    if(_type == "speaker"){
        Peer.addEventListener('track', e => gotRemoteStream(e, _userId));
    }
    return Peer;
}

function gotRemoteStream(e, index){
    PeerConnections[index].mediaStream = e.streams[0];
    console.log(PeerConnections[index].mediaStream.getVideoTracks().length);
    console.log("got remote stream");
    console.log("----------------------------------------");
}

function onIceCandidate(type, pc, event, userId, iceCandidateId) {
    if(event.candidate){
        var jsonString = event.candidate.candidate;
        //console.log(event.candidate);
        var candidate = event.candidate.candidate + "|" + event.candidate.sdpMLineIndex + "|" + event.candidate.sdpMid;
        var candidateMsg = "{\"Type\":2, \"Data\":\"" + Buffer.from(candidate).toString('base64') + "\", \"IceDataSeparator\": \"|\" }";
        PeerConnections[userId].iceCandidates[iceCandidateId].candidate.push(candidateMsg);
        console.log("send candidate: " + candidateMsg);
        console.log("----------------------------------------");
    }
}

function CreateConnection(roomId, _bodyString){
    return new Promise((resolve) => {
        //console.log("body: " + userId + "; " + reg.body);
        var Data = JSON.parse(_bodyString);
        //let buff = new Buffer(Data.Data, 'base64');
        var userId = Data.userId;

        if(!Rooms[roomId]){
            Rooms[roomId] = {speakerIds: []};
        }



        let sdp = Buffer.from(Data.Data, 'base64').toString('utf-8');
        console.log("body: " + _bodyString);
        console.log("----------------------------------------");
        var RTCS = new DefaultRTCPeerConnection.RTCSessionDescription();
        RTCS.type = "offer";
        RTCS.sdp = sdp;
        PeerConnections[userId] = {Peers: [], iceCandidates: [{candidate:[]}]};
        let peerId = 0;
        if(Data.ConnectionType == "speaker"){
            peerId = userId;
            if(!CheckUserInRoom(roomId, userId)){
                Rooms[roomId].speakerIds.push(userId);
            }
        }else if(Data.ConnectionType == "admin"){
            peerId = Data.speakerId;
        }


        PeerConnections[userId].Peers[peerId] = CreatePeerConnection(Data.ConnectionType, userId, peerId);
        if(Data.ConnectionType == "admin"){
            try{
                PeerConnections[peerId].mediaStream.getTracks().forEach(track => PeerConnections[userId].Peers[peerId].addTrack(track, PeerConnections[peerId].mediaStream));
                //peerConnection2.addStream(localStream);
            }
            catch (e) {
                console.log(e);
            }
        }
        PeerConnections[userId].Peers[peerId].setRemoteDescription(RTCS).then(() => {
            PeerConnections[userId].Peers[peerId].createAnswer().then((answer) => {
                //console.log("answer:\n" + answer.sdp);
                var serverMsg = "{\"Type\":1, \"Data\":\"" + Buffer.from(answer.sdp).toString('base64')  + "\"}";
                PeerConnections[userId].Peers[peerId].setLocalDescription(answer).then(() => {
                    resolve(serverMsg);
                }).catch((e) => {
                    console.log("setLocalDescription: " + e);
                    serverMsg = "failed";
                    resolve(serverMsg);
                });

            }).catch((e) => {
                console.log(e);
            });
        });


    });

}
var Rooms = [];


var idvall = [];

var idCommingData = [];
const offerOptions = {
    offerToReceiveAudio: 1,
    offerToReceiveVideo: 1
};
let localStream;
var PeerConnections = [];
/*PeerConnections[0] = {Peer: null, iceCandidates: []};
PeerConnections[0].Peer = CreatePeerConnection("speaker", 0);*/


function CheckUserInRoom(roomId, userId){
    Rooms[roomId].speakerIds.forEach(element => {
        if(element == userId){
            return true;
        }
    });
    return false;
}

const options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
};

const app = Express();
https.createServer(options, app).listen(443);
http.createServer(app).listen(3000);

app.get('/', (reg, res) => {
    res.statusCode = 200;
    res.send("<html><body><h1>test</h1></body></html>");
})

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type, origin, accept');
    next();
});

app.use(bodyParser.raw({ limit: '10mb', type: () => true}))

app.get('/', (reg, res) => {
    res.statusCode = 200;
    res.send("<html><body><h1>test</h1></body></html>");
})

app.get('/user/:id', (reg, res) => {
    const userId = reg.params.id;
    res.statusCode = 200;
    res.send("<html><body><pre>test: " + idvall[userId] + "</pre></body></html>");
})

// ======================== SPEAKER =========================
app.post('/setConnection/:id', (req, res) => {
    const roomId = req.params.id;
    CreateConnection(roomId, req.body.toString()).then((result) => {
        console.log("answer: " + result);
        console.log("----------------------------------------");
        res.statusCode = 200;
        res.end(result);
    });
})

app.post('/iceCandidate/:id', (req,res) => {
    const userId = req.params.id;
    var Data = JSON.parse(req.body.toString());
    let peerId = userId;
    if(Data.ConnectionType == "admin"){
        peerId = Data.speakerId;
    }

    let candidate = Buffer.from(Data.Data, 'base64').toString('utf-8');
    console.log(candidate);
    var candidateArray = candidate.split("|");
    var iceCandidateJSON = "{\"candidate\":\"" + candidateArray[0] + "\", \"sdpMLineIndex\":" + candidateArray[1] + ", \"sdpMid\":\"" + candidateArray[2] + "\"}";
    console.log("got candidate: " + iceCandidateJSON);
    console.log("----------------------------------------");
    var iceCandidate = JSON.parse(iceCandidateJSON);
    if(PeerConnections[userId]){
        PeerConnections[userId].Peers[peerId].addIceCandidate(iceCandidate);
    }

    res.statusCode = 200;
    res.end("got candidate. Thanks!");
})


app.get('/iceCandidate/:id', (req,res) => {
    const userId = req.params.id;
    const peerId = req.query.PEER_ID;

    if(Array.isArray(PeerConnections[userId].iceCandidates[peerId].candidate) && PeerConnections[userId].iceCandidates[peerId].candidate.length > 0){
        console.log("PEER_ID: " + peerId);
        console.log("iceCandidates length: " + PeerConnections[userId].iceCandidates[peerId].candidate.length);
        res.statusCode = 200;
        const data = PeerConnections[userId].iceCandidates[peerId].candidate.shift();
        res.send(data);
    }else{
        res.statusCode = 404;
        res.end();
    }
})


app.get('/getSpeakersInRoom/:id', (req,res) => {
    const roomId = req.params.id;
    var result = "{\"speakers\": ";
    var roomSpeakersJson = JSON.stringify(Rooms[roomId].speakerIds);
    result += roomSpeakersJson + "}";
    res.statusCode = 200;
    res.end(result);
})

/*const server = app.listen(3000, () => {
    const address = server.address();
    console.log(`http://localhost:${address.port}\n`)
})*/

